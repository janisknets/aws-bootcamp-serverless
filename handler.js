export default async (event) => {
  const service = process.env?.SERVICE;
  const stage = process.env?.STAGE;
  const username = process.env?.USERNAME;
  const table = process.env?.ORDERS_TABLE_NAME;

  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: `${service}-${stage}-${username}-${table}`,
        input: process.env
      },
      null,
      2
    ),
  };
};
