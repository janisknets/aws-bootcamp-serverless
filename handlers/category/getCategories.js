import Category from "../../utils/dynamo/categories";

export default async (event) => {
  try {
    const categories = await Category.scan().exec();
    return {
      statusCode: 200,
      headers: {},
      body: JSON.stringify(categories),
      isBase64Encoded: false
    };
  } catch (error) {
    /* something terrible has happened */
    console.error(error);
    return {
      statusCode: 500,
      headers: {},
      body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
      isBase64Encoded: false
    };
  }
};
