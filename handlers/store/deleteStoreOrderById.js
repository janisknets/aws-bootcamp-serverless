import Order from "../../utils/dynamo/orders";

export default async (event) => {
  try {
    // validate the orderId
    const orderId = Number(event.pathParameters.orderId);
    if (orderId < 0 || Number.isNaN(orderId)) {
        return {
          statusCode: 400,
          body: JSON.stringify("Invalid ID supplied"),
        };
    }

    /* try to get order from table by id orderId*/
    const order = await Order.query("id").eq(orderId).exec();
    if (order === undefined || order.count === 0) {
      /* no order found in table */
      return {
        statusCode: 404,
        headers: {},
        body: "No Order found",
        isBase64Encoded: false
      };
    }

    //delete the order itself if it exists;
    const deletedOrder = await Order.delete(orderId);
    console.log(deletedOrder);
    return {
      statusCode: 200,
      body: JSON.stringify("deleted store order by id"),
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      headers: {},
      body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
      isBase64Encoded: false
    };
  }
};
