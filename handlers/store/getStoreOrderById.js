import Order from "../../utils/dynamo/orders";

export default async (event) => {
  try {
    const orderId = event.pathParameters.orderId;
    /* try to get order from table by id orderId*/
    const order = await Order.query("id").eq(orderId).exec();
    if (order === undefined || order.length < 1) {
      /* no order found in table */
      return {
        statusCode: 404,
        headers: {},
        body: "No Order found",
        isBase64Encoded: false
      };
    }
    /* all was good, let's send the order back to the user */
    return {
      statusCode: 200,
      headers: {},
      body: JSON.stringify(order[0]),
      isBase64Encoded: false
    };
  } catch (error) {
    /* something terrible has happened */
    console.error(error);
    return {
      statusCode: 500,
      headers: {},
      body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
      isBase64Encoded: false
    };
  }
};
