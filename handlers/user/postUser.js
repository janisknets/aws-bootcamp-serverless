import User from '../../utils/dynamo/users';
import crypto from "crypto";

export default async (event) => {
  try {
    const body = JSON.parse(event.body);

    const exists = await User.query("username").eq(body.username).exec();
    if(exists !== undefined && exists.count > 0) {
        return {
            statusCode: 400,
            body: "This username is already exists"
        };
    }

    const salt = crypto.randomBytes(16).toString('hex');
    const hash = crypto.pbkdf2Sync(body.password, salt, 1000, 64, `sha512`).toString(`hex`);
    delete body.password;

    const user = await User.create({ ...body, salt, hash });

    delete user.hash;
    delete user.salt;

    return {
      statusCode: 200,
      body: JSON.stringify(user),
    };
  } catch (error) {
    /* something terrible has happened */
    console.error(error);
    return {
        statusCode: 500,
        headers: {},
        body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
        isBase64Encoded: false
    };
  }
};
