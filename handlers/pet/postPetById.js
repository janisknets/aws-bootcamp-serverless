import Pet from '../../utils/dynamo/pets';

export default async (event) => {
  try {
    const name = event.formParamters?.name;
    const status = event.formParamters?.status;
    const petId = event.pathParameters?.petId;
    const body = { id: petId, status, name };
    if (!status) {
        delete body.status;
    }
    if (!name) {
        delete body.name;
    }
    if(!status && !name ) {
        return {
            statusCode: 400,
            body: "nothing to update"
        };
    }
    const pet = await Pet.update(body);

    return {
      statusCode: 200,
      body: JSON.stringify(pet),
    };
  } catch (error) {
    /* something terrible has happened */
    console.error(error);
    return {
        statusCode: 500,
        headers: {},
        body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
        isBase64Encoded: false
    };
  }
};
