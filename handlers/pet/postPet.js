import Pet from '../../utils/dynamo/pets';

export default async (event) => {
  try {
    const body = JSON.parse(event.body);

    const pet = await Pet.create(body);

    return {
      statusCode: 200,
      body: JSON.stringify(pet),
    };
  } catch (error) {
    /* something terrible has happened */
    console.error(error);
    return {
        statusCode: 500,
        headers: {},
        body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
        isBase64Encoded: false
    };
  }
};
